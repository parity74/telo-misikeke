#!/usr/bin/env node

(async () => {
  const args = require('yargs')
        .usage('Usage: $0 [options] [IN_FILES...] ')
        .alias('h', 'help')
        .describe('l', 'Line breaks form new sentences').boolean('l').alias('l', 'linebreaks')
        .describe('p', 'Show possible errors').boolean('p').alias('p', 'possible-errors')
        .describe('n', 'Show nitpicks').boolean('n').alias('n', 'nitpicks')
        .parse();

  // Source directory 
  const src = "../public";
  
  [ require('node:fs'),
    require(`${src}/rules.js`),
    require(`${src}/Parser.js`),
  ].forEach(e => { Object.assign(module.exports, e) })
  
  rules = this.build_rules(this.parseLipuLinku(require(`${src}/linku.json`)))
  
  var parser = new (this.ParserWithCallbacks)(rules, args.l);

  getMessage = function(key, match) {
    if(!(key in rules))
      return false;

    let err = rules[key]
    let message = err.message;

    if(typeof(message) == 'function') {
      message = message(match);
    }

    for(var i=1; i<match.length; i++) {
      message = message.replace(new RegExp('\\$'+(i-1), 'g'), match[i]);
    }

    if(err.more_infos) {
      // message += '<br><a  class="more-infos" target="_blank" href="' + err.more_infos + '">[Learn more]</a>';
    }
    
    // Strip formatting tags
    return message.replace(/<\/?("[^"]*"|'[^']*'|[^>])*(>|$)/g, "");
  };
  
  let ignore = {
    "error": false,
    "nitpick": !args["nitpicks"],
    "possible-error": !args["possible-errors"],
  };

  let sources = [];
  let text = "";
  let stdin = "";
  
  if(args._.length == 0) {
    sources = ["-"]
    for await (const chunk of process.stdin) stdin += chunk;
  }
  else {
    sources = args._.sort()
  }
  
  // Check each file
  sources.forEach(
    filename => {
      
      text = filename == "-" ? stdin : this.readFileSync(filename, 'utf8')

      // Indent with a single space
      text = text.replaceAll("\n", "\n ")
      
      tokens = parser.tokenize(text)

      countlines = (str) => ((matches => matches ? matches.length : 0)(str.match(/\n/g)))
      linenumber = (arr) => countlines(arr.map(e => e.text).join(''))
      
      response = tokens.reduce(
        (acc,token,idx,arr) => {

          if(rules[token.ruleName] ? rules[token.ruleName].category : false) {
            // Add header line to each error
            prefix = [];
            suffix = [];

	    // Read backward to place heading at start of line
            // Assign same error to prefix
            while( arr[idx-prefix.length-1] && arr[idx-prefix.length-1].text.search("\n") < 0){
	      prefix = [
		Object.assign( {}, arr[idx-prefix.length-1], { ruleName: token.ruleName } ),
		...prefix
	      ];
            }
	    
            // Read forward to end of line
            // Assign same error to prefix
            if(token.text.search("\n") != token.text.length-1){
              while(suffix.length == 0 || ( suffix[suffix.length-1].text && suffix[suffix.length-1].text.search("\n") < 0 )){
                suffix = [
		  ...suffix,
		  Object.assign( {}, arr[idx+suffix.length+1], { ruleName: token.ruleName } )
		];
              }
            }
	    
            return [ ...acc,
                     { ruleName: token.ruleName, text: `@@ -${linenumber(arr.slice(0,idx))},${countlines(token.text)} +${linenumber(arr.slice(0,idx))},${countlines(token.text)} @@ ${getMessage(token.ruleName,token.match).split('\n').join(" ")}\n` },
                     ...prefix, token, ...suffix];
          }
          else {
            // no error
            return [...acc, token];
          }
          
        }, [""]
      )
      
        .filter(e => (
          // Filter out lines with no error      
          !rules[e.ruleName] ? false
          // Remove ignored errors
            : rules[e.ruleName].category ? !ignore[rules[e.ruleName].category]
            : false ) )

      // Log if any errors
      if(response.length > 0) {
        console.log(`--- a/${filename}`)
        console.log(`+++ b/${filename}`)
        
        console.log(response.map(e => e.text).join(''))
      }
    }
  );
  
})()
